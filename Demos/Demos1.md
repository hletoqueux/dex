# Example ONE - Import, visualise and cleanse some data

### Introduction
We will use the BL-Flickr-Images-Book.csv dataset to demonstrate some capacities of OpenRefine : 

- Quickly understand how to dive into a dataset using this tool
- Cluster some value
- Create a new column in the dataset using GREL, the native language of OpenRefine
- Extract a subset of the dataset.


### Import the data

- In the main window, we create a new project by importing the csv dataset, and click on __next__.

![Image of main window](images/1-1.png)

- The following window is a preview of your project where you can change its name, add some tags and customize some options.

![Image of main window](images/1-2.png)

- The last windows shows you the actual project containing 8287 rows.

![Image of main window](images/1-3.png)

### A quick glance

As you can notice : 
 - the column __Date of Publication__ contains dates but also approximations, ranges.... 
 - the column __Place of Publication__ contains typos, irrelevant information...
 - the project has empty thus useless columns...
 - ...

This project is messy, just like a standard dataset! Let's cleanse it!

### Clustering

We want to quickly cluster the places and correct typos inside this column.

- In the vertical menu of the column, click and select the clustering menu.

![Image of main window](images/1-4.png)

- By default OpenRefine will use the __fingerprint__ algorithm to search matching values (but you can choose other algorithms). Here, for instance, __London__ has several typos. By ticking the box, and using __merge selected & re-cluster__ these values will be replaced by "London".

![Image of main window](images/1-5.png)

- repeat these operations on all the values you have, using other algorithm.

![Image of main window](images/1-6.png)

### Extract a subset by filtering the date of publication

Let's say that we only want the items published in 1897.

- In the vertical menu of the column Date of Publication, click and select facet > text facet option. This will open a facet window on the left. Click on __count__ to facet by descending count.

![Image of main window](images/1-7.png)

- Click on 1897. This will only display the 157 matching rows, a subset of the main (8287) dataset.

![Image of main window](images/1-8.png)

- Unclick on 1897 to get back to the previous window.

### Extract informations from a column to a new column

Let's say that we want to create a column called "Bracketed" with date-in-between-brackets in the column __Date of Publication__.
We are going to use the regexp __/\[.*\]/__ (i-e meaning anything between two brackets), and a GREL expression : __"partition"__

This command will break a string into three pieces : 

- The pivot : in our case it's the regexp __/\[.*\]/__. The pivot is always shown as "[1]"
- The left part of the pivot : all the characters that are on the left are shown as "[0]"
- The right part of the pivot : all the characters that are on the right are shown as "[2]"

E.g.: In the sentence "You know what? I love OpenRefine", if "love" is our pivot, then :
* value[1] contains "love"
* value[0] contains "You know what? I "
* value[2] contians " OpenRefine"

In our case we will use : ```value.partition( /\[.*\]/)[1]``` (we have to use backslash in order to escape the bracket characters)

- In the vertical menu of the column Date of Publication, click and select the menu __edit column/add column based on this column__.

![Image of main window](images/1-9.png)

- Name this column "Bracketed", replace ```value``` by ```value.partition( /\[.*\]/)[1]```, click on OK.

![Image of main window](images/1-10.png)


## Conclusion

These are just simple examples to show you some interesting possibilities offered by OpenRefine to cleanse a dataset.

Let's apply this to OSINT with our [second example](Demos2.md)!
