# Example FIVE - Enrich our data using osint TOOLS


### Introduction

On this example, we will investigate INSTAGRAM accounts using the tool [Toutatis](https://github.com/megadose/toutatis), developped by [@palenath](https://twitter.com/palenath). you'll need a valid instagram session to do that.

The command is ```toutatis -s instagramsessionid -u userid```.

Please refer to the toutatis page on github to find your own instagramsessionid.

The dataset *instagram.csv* consists of three random accounts (it could be much more...).

The command is pretty

### Import the data

- Create a project in OpenRefine using this csv file.

- Create a column based on Column 1, using this expression : ```"toutatis -s instagramsessionid -u "+value```

- Execute the script.

![Image of main window](images/5-1.png)

### Parse the data

The result is a raw text, so we have to manipulate it a little bit : 

- replace the invisible \n (*newline*), by a comma : ```value.replace("\n",",")```
- remove the extra spaces in the text : ```value.replace(/\s+/,' ')```

Then create your desired columns like this : 

- userid : ```value.partition('userID : ')[2].partition(',')[0]```
- followers : ```value.partition('Follower : ')[2].partition(' |')[0]```
- etc...

![Image of main window](images/5-2.png)


### Be Lazy!!

 - use the history of your commands
 - star the useful commands

![Image of main window](images/5-3.png)
